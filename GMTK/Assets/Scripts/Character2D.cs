﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Character
{
    public class Character2D : MonoBehaviour
    {
        // Start is called before the first frame update

        // Movement prefs
        [SerializeField] private float m_MaxSpeed = 10f;
        [SerializeField] private float m_JumpForce = 400f;
        [SerializeField] private bool m_AirControl = true; 

        //Collision
        [SerializeField] public LayerMask m_WhatIsGround;
        private Transform m_GroundCheck;
        const float k_GroundedRadius = .2f;
        [HideInInspector]public bool m_Grounded; 

        //Animation
        private Animator m_Animator;
        //Sound
        public GameObject theme;
        private AudioManager audio;
        //State
        private Rigidbody2D m_RigidBody;
        public bool m_FacingRight = true;
        private bool m_UpsideDown = true;
        //Death
        public bool fallingDamage = true;
        private bool m_lethalSpeedCheck = false;
        public float m_lethalFallSpeed = 15f;
        
        private bool m_dying = false;
        private bool firstgroundtouch = true;


        private void Awake() {
            //lock framerate at 60
            Application.targetFrameRate = 60;

            //Setup References
            m_GroundCheck = transform.Find("GroundCheck");
            m_Animator = GetComponent<Animator>();
            m_RigidBody = GetComponent<Rigidbody2D>();
            audio = GetComponent<AudioManager>();

        }

        private void FixedUpdate() {
            m_Grounded = false;
            //check to see if on the ground
            Collider2D[] groundColliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < groundColliders.Length; i++)
            {
                //check to make sure it's not colliding with itself
                if(groundColliders[i].gameObject != gameObject){
                    m_Grounded = true;
                    
                }
            }

            //[GTK-98] Setup Animation bools
            m_Animator.SetFloat("vSpeed", m_RigidBody.velocity.y);
            //[GTK-99] Falling Damage
            if(m_lethalSpeedCheck && m_Grounded){
                Kill();
                m_lethalSpeedCheck = false;
            }
            if(!m_Grounded) {
                float currentSpeed = Mathf.Abs(m_RigidBody.velocity.y);

                if(currentSpeed > m_lethalFallSpeed && fallingDamage){
                    m_lethalSpeedCheck = true;
                } else {
                    m_lethalSpeedCheck = false;
                }
            }
            m_Animator.SetBool("ground", m_Grounded);
        }

        public void Move(float move, bool jump){
            if(m_dying) {
                return;
            }

            if(m_Grounded || m_AirControl) {
                //set speeds
                m_Animator.SetFloat("xSpeed", Math.Abs(move));
                m_RigidBody.velocity = new Vector2(move*m_MaxSpeed, m_RigidBody.velocity.y);

                //check to see if we should flip the player
                if (move > 0 && !m_FacingRight)
                {
                    Flip(true);
                }
                
                if(move < 0 && m_FacingRight){
                    Flip(true);
                }

            }
            if(m_Grounded && jump){
                Jump();
                m_Animator.SetTrigger("jump");
            }
        }

        public void Jump () {
            float jforce = m_JumpForce;
            m_RigidBody.AddForce(new Vector2(0f,jforce));
            //add in jump audio
        }

        //flips the orientation of the character
        // true = x
        // false = y
        private void Flip(bool direction){
            Vector3 theScale = transform.localScale;
            if(direction){
                theScale.x *= -1;
                m_FacingRight = !m_FacingRight;
            } else {
                m_UpsideDown = !m_UpsideDown;
                theScale.y *= -1;
            }
            transform.localScale  = theScale;
        }

        private void Kill() {
            if(m_dying) return;
            Debug.Log("Dying");
            m_Animator.SetBool("death",true);
            m_dying = true;
            theme.GetComponent<AudioSource>().Stop();
            audio.m_AudioSource.PlayOneShot(audio.m_Death,audio.m_DeathVol);
        }
        public void resetLevel(){
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
