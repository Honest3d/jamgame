using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Character
{
    [RequireComponent(typeof (Character2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private Character2D m_Character;
        private ControlButtons m_ctrlButtonFunc;
        private bool m_Jump;
        private bool m_Interact;
        private bool m_ctrl_press;


        private void Awake()
        {
            m_Character = GetComponent<Character2D>();
            m_ctrlButtonFunc = GetComponent<ControlButtons>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

            if(m_ctrl_press) {
                if(Input.GetKeyDown(KeyCode.C)) {
                    m_ctrlButtonFunc.Copy();
                }
                if(Input.GetKeyDown(KeyCode.X)) {
                    m_ctrlButtonFunc.Cut();
                }
                if(Input.GetKeyDown(KeyCode.V)) {
                    m_ctrlButtonFunc.Paste();
                }
                if(Input.GetKeyDown(KeyCode.Z)) {
                    m_ctrlButtonFunc.Undo();
                }
            }
        }


        private void FixedUpdate()
        {

            m_ctrl_press = Input.GetKey(KeyCode.LeftControl);
            
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            m_Character.Move(h, m_Jump);
            m_Jump = false;
            m_Interact = false;
        }
    }
}
