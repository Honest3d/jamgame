﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private SpriteRenderer sprite;
    public bool open = false;
    [Range(0.0f,2.0f)] public float transitionTime = 1f;

    private BoxCollider2D m_Collider;

    

    private void Start() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = new Color(1f,1f,1f,!open ? 1f : 0f );
        m_Collider = GetComponent<BoxCollider2D>();
    }
    private void Update() {
        m_Collider.enabled = !open;    
    }
    public void Toggle(){
        open = !open;
        StartCoroutine(fadeInAndOut(gameObject,!open,transitionTime));
    }

    public void Set(bool val){
        open = val;
        StartCoroutine(fadeInAndOut(gameObject,!open,transitionTime));
    }


    IEnumerator fadeInAndOut(GameObject objectToFade, bool fadeIn, float duration)
    {
        float counter = 0f;

        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0;
            b = 1;
        }
        else
        {
            a = 1;
            b = 0;
        }

        Color currentColor = Color.clear;
        SpriteRenderer tempSPRenderer = objectToFade.GetComponent<SpriteRenderer>();
        currentColor = tempSPRenderer.color;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);   
            tempSPRenderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);           
            yield return null;
        }
    }
}
