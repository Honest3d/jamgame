﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public int NumberOfNeededEntities=1;
    public GameObject door;
    public bool lockingPlatform;
    public int collidingObjects = 0;
    private bool platformState = false;
    public bool invertOutput = false;
    public bool changeSprite = true;
    public Sprite platformOff;
    public Sprite platformOn;
    
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = platformOff;
    }

    // Update is called once per frame
    void Update()
    {
        if(invertOutput){
            if(collidingObjects >= NumberOfNeededEntities){
                door.GetComponent<DoorManager>().Set(false);
                platformState = true;
            } else if(!door.GetComponent<DoorManager>().open && !lockingPlatform) {
                door.GetComponent<DoorManager>().Set(true);
                platformState = false;
            }
        } else {

            if(collidingObjects >= NumberOfNeededEntities){
                door.GetComponent<DoorManager>().Set(true);
                platformState = true;
            } else if(door.GetComponent<DoorManager>().open && !lockingPlatform) {
                door.GetComponent<DoorManager>().Set(false);
                platformState = false;
            }
        }
        if(changeSprite){
            spriteRenderer.sprite = platformState ? platformOn : platformOff;
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player") || other.CompareTag("Clone")){
            collidingObjects++;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player") || other.CompareTag("Clone")){
            collidingObjects--;
        }
    }
}
