﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManagerMoving : MonoBehaviour
{
    public int NumberOfNeededEntities=1;
    public bool lockingPlatform;
    public int collidingObjects = 0;
    public float amtToMovePerFrame = 0.01f;
    private float posInMove = 0f;
    public Transform StartingPosition;
    public Transform FinishingPosition;
    private bool platformState = false;
    public bool changeSprite = true;
    public Sprite platformOff;
    public Sprite platformOn;
    
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = platformOff;
        transform.position = StartingPosition.position;
    }

    // Update is called once per frame
    void Update()
    {    
        platformState = collidingObjects >= NumberOfNeededEntities;
        if(platformState && posInMove < 1){
            posInMove+=amtToMovePerFrame;
        }
        if(!platformState && posInMove>0) {
            posInMove-=amtToMovePerFrame;
        }
        if(changeSprite){
            spriteRenderer.sprite = platformState ? platformOn : platformOff;
        }

        transform.position = Vector2.Lerp(StartingPosition.position,FinishingPosition.position, posInMove);

    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player") || other.CompareTag("Clone")){
            collidingObjects++;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player") || other.CompareTag("Clone")){
            collidingObjects--;
        }
    }
}
