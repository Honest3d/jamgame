﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class Parallax : MonoBehaviour {
    //multiplier for parallax effect
    [SerializeField] private Vector2 parallaxEffectMultiplier;
    //main camera
    [SerializeField] private Camera mainCamera;
    private Transform cameraTransform;
    private Vector3 lastCameraPosition;
    private void Start()
    {
        //get camera positioning
        cameraTransform = mainCamera.transform;
        lastCameraPosition = cameraTransform.position;
    }

    private void LateUpdate()
    {
        
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        transform.position += new Vector3(deltaMovement.x * parallaxEffectMultiplier.x, deltaMovement.y * parallaxEffectMultiplier.y, 0);
        lastCameraPosition = cameraTransform.position;
    }
}
