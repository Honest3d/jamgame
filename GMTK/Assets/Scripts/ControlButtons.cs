﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Character
{
    public class ControlButtons : MonoBehaviour
    {
        //prefs
        public GameObject m_cutOutlineSprite;
        public GameObject m_copyObject;
        public GameObject m_saveObjectSprite;
        public int m_CopySpawnDistance = 1;
        public int m_MaxCommands = 10;
        public DialogueTrigger dialogueTrigger;
        
        //references
        private Animator m_Animator;
        private Character2D m_charMgr;
        private AudioManager m_audio;

        //state
        private GameObject m_pasteObject = null;
        private bool m_pasteFlipX = false;
        
        private int m_savedCommandCounter = -1;
        private int m_CommandCounter;

        public GameObject ctrl;
        private ctrlBar ctrlBarObj;

        private void Start() {
            m_CommandCounter = m_MaxCommands;    
            m_Animator = GetComponent<Animator>();
            m_charMgr = GetComponent<Character2D>();
            m_audio = GetComponent<AudioManager>();
            ctrl = GameObject.Find("ctrlBar");
            ctrlBarObj = ctrl.GetComponent<ctrlBar>();
            ctrlBarObj.setMaxCtrl(m_MaxCommands);
            ctrlBarObj.setCtrl(m_CommandCounter);
            Save();

        }
        private void Update() {
            if(ctrlBarObj == null) {
                return;
            }
            ctrlBarObj.setCtrl(m_CommandCounter);
        }
        public void Copy(){
            if(!checkCommandValid()) {
                return;
            }

            Vector2 pos = transform.position;
            //TODO: Spawn Copy accepts direction
            if(m_charMgr.m_FacingRight) {
                pos.x += m_CopySpawnDistance;
            } else {
                pos.x -= m_CopySpawnDistance;
            }
            pos.y+=0.2f;
            bool colliding = false;
            
            Collider2D[] copyColliders = Physics2D.OverlapCircleAll(pos, .2f, m_charMgr.m_WhatIsGround);
            for (int i = 0; i < copyColliders.Length; i++)
            {
                //check to make sure it's not colliding with itself
                if(copyColliders[i].gameObject != gameObject){
                    colliding = true;
                }
            }
            
            if(colliding) {
                pos = transform.position;
                if(m_charMgr.m_FacingRight) {
                    pos.x -= m_CopySpawnDistance;
                } else {
                    pos.x += m_CopySpawnDistance;
                }
                pos.y+=0.2f;
                colliding = false;
            }

            Collider2D[] copyColliders2 = Physics2D.OverlapCircleAll(pos, .2f, m_charMgr.m_WhatIsGround);
            for (int i = 0; i < copyColliders2.Length; i++)
            {
                //check to make sure it's not colliding with itself
                if(copyColliders2[i].gameObject != gameObject){
                    colliding = true;
                }
            }

            if(colliding) {
                return;
            }

            
            Instantiate(m_copyObject, pos, transform.rotation);
            m_CommandCounter--;
            checkControlHalf();
            m_Animator.SetTrigger("copy");
            m_audio.m_AudioSource.PlayOneShot(m_audio.m_Copy,m_audio.m_CopyVol);
        }

        public void Cut() {
            if(!checkCommandValid()) {
                return;
            }
            if(m_pasteObject != null){
                Destroy(m_pasteObject);
            }
            m_pasteObject = Instantiate(m_cutOutlineSprite, transform.position, transform.rotation) as GameObject;
            m_pasteFlipX = m_charMgr.m_FacingRight;
            m_pasteObject.transform.localScale = transform.localScale;
            m_CommandCounter--;
            checkControlHalf();
            m_Animator.SetTrigger("copy");
            m_audio.m_AudioSource.PlayOneShot(m_audio.m_Cut,m_audio.m_CutVol);

        }

        public void Paste() {
            if(!checkCommandValid()) {
                return;
            }
            if (m_pasteObject != null) {
                transform.position = m_pasteObject.transform.position;
                transform.localScale = m_pasteObject.transform.localScale;
                m_charMgr.m_FacingRight = m_pasteFlipX;
                m_CommandCounter--;
                checkControlHalf();
                m_audio.m_AudioSource.PlayOneShot(m_audio.m_Paste,m_audio.m_PasteVol);
            } else {
                m_audio.m_AudioSource.PlayOneShot(m_audio.m_Err,m_audio.m_ErrVol);
            }
        }

        public void Save() {
            m_savedCommandCounter = m_CommandCounter;
        }

        public void Undo() {
            m_charMgr.resetLevel();
        }

        private bool checkCommandValid() {
            if(m_CommandCounter >= 1 && m_charMgr.m_Grounded) {
                return true;
            } else {
                //TODO: [GTK-106] Add error sound/splash for incorrect/invalid command.
                m_audio.m_AudioSource.PlayOneShot(m_audio.m_Err,m_audio.m_ErrVol);
                return false;
            }
            

        }
   public void checkControlHalf()
    {
            if(m_CommandCounter<=(m_MaxCommands/2))
            {
                dialogueTrigger.TriggerDialogue();
            }
    }
    }
}