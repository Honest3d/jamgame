﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    private Queue<string> sentences;
    private Queue<int> faces;
    public Text dialogueText;
    public Animator animator;
    public Animator faceAnimation;
    private AudioManager audioManager;
    private bool isOpen;
    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.Find("MainCharacter").GetComponent<AudioManager>();
        sentences = new Queue<string>();
        faces = new Queue<int>();
        isOpen=false;
    }
    void Update()
    {
        if(Input.GetButtonDown("Interact")&&isOpen){
            DisplayNextSentence();
        }
    }
    public void StartDialogue(Dialogue dialogue)
    {
        isOpen=true;
        animator.SetBool("open", isOpen);
        sentences.Clear();
        faces.Clear();
        foreach(string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        foreach(int face in dialogue.faces)
        {
            faces.Enqueue(face);
        }
        DisplayNextSentence();
        //Debug.Log("step 2");
    }
    public void DisplayNextSentence(){
        if(sentences.Count == 0)
        {
            isOpen=false;
            EndDialogue();
            return;
        } else {
            animator.SetTrigger("shake");
            string sentence = sentences.Dequeue();
            int face = faces.Dequeue();
            audioManager.ClippySound(face<4);
            faceAnimation.SetInteger("face",face);            
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        }
    }

    IEnumerator TypeSentence(string sentence){
        dialogueText.text = "";
        foreach(char letter in sentence.ToCharArray()){
            dialogueText.text += letter;
            yield return null;
        }
    }
    public void EndDialogue(){
        animator.SetBool("open", isOpen);
        Debug.Log("End of Dialogue");
    }
}
