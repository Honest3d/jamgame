﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class PullOutStarter : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Timeline;
    private PlayableDirector director;
    
    void Start()
    {
        director = Timeline.GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other) {
            director.Play();
    }
}
