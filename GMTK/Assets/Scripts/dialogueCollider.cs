﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueCollider : MonoBehaviour
{
    public Collider2D thisCollider;
    public DialogueTrigger dialogueTrigger;
    void start(){
        dialogueTrigger = new DialogueTrigger();
        thisCollider.enabled = true;
    }
    private void OnTriggerEnter2D(Collider2D other){
        if(other.CompareTag ("Player")){
            dialogueTrigger.TriggerDialogue();
            thisCollider.enabled = false;
            //Debug.Log("triggered");
        }
    } 
}
