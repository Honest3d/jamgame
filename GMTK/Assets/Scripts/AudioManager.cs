﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [HideInInspector]public AudioSource m_AudioSource;
    public AudioClip m_Copy;
    [Range(0f,1f)]public float m_CopyVol;
    public AudioClip m_Cut;
    [Range(0f,1f)]public float m_CutVol;
    public AudioClip m_Paste;
    [Range(0f,1f)]public float m_PasteVol;
    public AudioClip m_Save;
    [Range(0f,1f)]public float m_SaveVol;
    public AudioClip m_Undo;
    [Range(0f,1f)]public float m_UndoVol;
    public AudioClip m_Err;
    [Range(0f,1f)]public float m_ErrVol;

    public AudioClip m_Death;
    [Range(0f,1f)]public float m_DeathVol;

    public AudioClip m_clippyHappy;
    [Range(0f,1f)]public float m_clippyHappyVol;

    public AudioClip m_clippyAngry;
    [Range(0f,1f)]public float m_clippyAngryVol;
    
    // Start is called before the first frame update
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }
    public void ClippySound(bool happy) {
        if(happy) {
            m_AudioSource.PlayOneShot(m_clippyHappy,m_clippyHappyVol);
        } else {
            m_AudioSource.PlayOneShot(m_clippyAngry,m_clippyAngryVol);
        }
    }
    
}
