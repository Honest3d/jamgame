﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ctrlBar : MonoBehaviour
{
    public Slider slider;
    public Text text;
    private int control;
    private int maxControl;

    public void setCtrl(int ctrl)
    {
        control=ctrl;
        slider.value = ctrl;
        text.text = "Ctrl:" + control + "/" + maxControl;
    }
    public void setMaxCtrl(int maxCtrl)
    {
        maxControl=maxCtrl;
        slider.maxValue = maxCtrl;
        text.text = "Ctrl:" + control + "/" + maxControl;
    }
}
